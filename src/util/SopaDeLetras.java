package util;

import java.io.*;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import javax.swing.JOptionPane;
import vista.Interfaz;

public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];
    Interfaz Texto = new Interfaz();
    String palabra;
//
//    /**
//     * Constructor for objects of class SopaDeLetras
//     */

    public SopaDeLetras() {

    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }

    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String leerExcel(String Nombre) throws Exception {

        HSSFWorkbook Excel = new HSSFWorkbook(new FileInputStream(Nombre));
        HSSFSheet hoja = Excel.getSheetAt(0);

        int Filas = hoja.getLastRowNum() + 1;
        int Columnas = 0;
        String valor = "";

        sopas = new char[Filas][];
        for (int i = 0; i < Filas; i++) {
            HSSFRow fil = hoja.getRow(i);
            Columnas = fil.getLastCellNum();
            sopas[i] = new char[Columnas];
            for (int j = 0; j < Columnas; j++) {
//                sopas[i][j] = fil.getCell(j).getStringCellValue().charAt(0);
                valor += fil.getCell(j).getStringCellValue();
                
            }
            valor += "\n";
        }
        return valor;
    }
   
//    public void llenar(HSSFRow fila, int numFila){
//        
//        String palabra = "";
//        
//        for(int i=0; i<fila.getLastCellNum();i++){
//            
//            palabra = palabra + fila.getCell(i);
//        
//        }
//        
//        char [] Fil = palabra.toCharArray();
//        
//        for(int j=0; j<sopas[numFila].length; j++){
//            sopas[numFila][j] = Fil[j]; 
//            System.out.println(sopas+"");
//        }  
//    }
//    
//    public void guardarPdf(){
//        
//        try{
//            
//            OutputStream guardar = new FileOutputStream(Texto.seleccionar.getSelectedFile());
//            
//        
//        }
//        catch(Exception e){JOptionPane.showMessageDialog(null, "No se ha podido guardar el archivo correctamente");}
//    
//    }
    public boolean esCuadrada() throws Exception {
        if (sopas == null) {
            throw new Exception("la matriz esta vacia");
        }

        boolean cuadrada = true;
        for (int i = 0; i < sopas.length; i++) {
            if (sopas[i].length != sopas.length) {
                return false;
            }
        }
        return cuadrada;
    }

    public boolean esDispersa() throws Exception {
        if (sopas == null) {
            throw new Exception("la matriz esta vacia");
        }

        int j = 1;
        boolean dispersa = true;
        int contador = 0;
        for (int i = 0; i + 1 < sopas.length; i++) {
            if (j != sopas.length) {
                if (sopas[i].length != sopas[j].length) {
                    contador++;
                }
            }
            j++;
        }
        dispersa = contador != 0;

        return dispersa;
    }

    public boolean esRectangular() throws Exception {
        if (sopas == null) {
            throw new Exception("La matriz esta vacia");
        }

        if (!esDispersa() && !esCuadrada()) {
            return true;
        } else {
            return false;
        }
    }

    public int[] izquierdaADerecha(String palabra) {

        int respuesta[] = new int[contadorHorizontal(palabraNormal(palabra)) * 2];
        int contador = 0;
        int k = 0;
        int mp = 0;
        int con = 0;
        boolean saber1;

        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {

                if (palabraNormal(palabra)[mp] == sopas[i][k]) {
                    contador++;
                    if (mp != palabraNormal(palabra).length - 1) {
                        mp++;
                    }
                    if (i != sopas.length - 1) {

                        if ((sopas[i + 1][0] == palabraNormal(palabra)[mp]
                                || sopas[i + 1][0] == palabraNormal(palabra)[0])
                                && contador > 0
                                && k == sopas[i].length - 1
                                && contador != palabraNormal(palabra).length) {
                            contador = 0;
                            mp = 0;
                        }
                    }
                    if (k != sopas[i].length - 1) {

                        if ((sopas[i][k] == sopas[i][k + 1]
                                || sopas[i][k + 1] == palabraNormal(palabra)[0])
                                && contador < palabraNormal(palabra).length) {
                            contador = 0;
                            mp = 0;
                        }

                    }
                } else {
                    contador = 0;
                    mp = 0;
                }
                if (k != sopas[i].length - 1) {
                    saber1 = sopas[i][k] == sopas[i][k + 1] && contador <= 1;
                    if (saber1) {
                        contador = 0;
                        mp = 0;
                    }
                }

                if (contador == palabraNormal(palabra).length) {

                    respuesta[con] = i;
                    respuesta[con + 1] = k - (palabraNormal(palabra).length - 1);

                    con = con + 2;
                    contador = 0;
                    mp = 0;
                }
                k++;
            }
            k = 0;
        }
        return respuesta;
    }

    public int[] derechaIzquierda(String palabra) {
        int respuestaAlrevez[] = new int[contadorHorizontal(palabraAlrevez(palabra)) * 2];
        int contador = 0;
        int k = 0;
        int mp = 0;
        int con = 0;

        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {

                if (palabraAlrevez(palabra)[mp] == sopas[i][k]) {
                    contador++;
                    if (mp != palabraAlrevez(palabra).length - 1) {
                        mp++;
                    }
                    if (i != sopas.length - 1) {

                        if ((sopas[i + 1][0] == palabraAlrevez(palabra)[mp]
                                || sopas[i + 1][0] == palabraAlrevez(palabra)[0])
                                && contador > 0
                                && k == sopas[i].length - 1
                                && contador != palabraAlrevez(palabra).length) {
                            contador = 0;
                            mp = 0;
                        }
                    }
                    if (k != sopas[i].length - 1) {

                        if ((sopas[i][k] == sopas[i][k + 1]
                                || sopas[i][k + 1] == palabraAlrevez(palabra)[0])
                                && contador < palabraAlrevez(palabra).length) {
                            contador = 0;
                            mp = 0;
                        }

                    }
                } else {
                    contador = 0;
                    mp = 0;
                }

                if (contador == palabraAlrevez(palabra).length) {

                    respuestaAlrevez[con] = i;
                    respuestaAlrevez[con + 1] = k - (palabraAlrevez(palabra).length - 1);

                    con = con + 2;
                    contador = 0;
                    mp = 0;

                }

                k++;

            }
            k = 0;
        }

        return respuestaAlrevez;
    }

    private int contadorHorizontal(char vector[]) {
        int contador = 0;
        int hecho = 0;
        int k = 0;

        int mp = 0;

        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {

                if (vector[mp] == sopas[i][k]) {
                    contador++;
                    if (mp != vector.length - 1) {
                        mp++;
                    }

                    if (i != sopas.length - 1) {

                        if ((sopas[i + 1][0] == vector[mp]
                                || sopas[i + 1][0] == vector[0])
                                && contador > 0
                                && k == sopas[i].length - 1
                                && contador != vector.length) {
                            contador = 0;
                            mp = 0;
                        }
                    }
                    if (k != sopas[i].length - 1) {

                        if ((sopas[i][k] == sopas[i][k + 1]
                                || sopas[i][k + 1] == vector[0])
                                && contador < vector.length) {
                            contador = 0;
                            mp = 0;
                        }

                    }

                } else {
                    contador = 0;
                    mp = 0;
                }

                if (contador == vector.length) {
                    hecho++;
                    contador = 0;
                    mp = 0;
                }

                k++;

            }
            k = 0;
        }

        return hecho;
    }

    public int[] arribaAAbajo(String palabra) {
        int respuesta[] = new int[contadorVertical(palabraNormal(palabra)) * 2];
        int contador = 0;

        int k = 0;
        int mp = 0;
        int con = 0;
        int ayuda = 0;

        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {
                if (palabraNormal(palabra)[mp] == sopas[i + ayuda][k]) {
                    contador++;

                    if (mp != palabraNormal(palabra).length - 1) {
                        mp++;
                    }
                    if (ayuda + i != sopas.length - 1) {
                        ayuda++;
                    }
                } else {
                    contador = 0;
                    ayuda = 0;
                    mp = 0;
                    k++;
                }

                if (contador == palabraNormal(palabra).length) {
                    ayuda = 0;
                    respuesta[con] = i;
                    respuesta[con + 1] = k;

                    con = con + 2;
                    contador = 0;
                }

            }
            k = 0;

        }

        return respuesta;
    }

    public int[] AbajoAArriba(String palabra) {
        int respuesta[] = new int[contadorVertical(palabraAlrevez(palabra)) * 2];
        int contador = 0;

        int k = 0;
        int mp = 0;
        int con = 0;
        int ayuda = 0;

        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {
                if (palabraAlrevez(palabra)[mp] == sopas[i + ayuda][k]) {
                    contador++;

                    if (mp != palabraAlrevez(palabra).length - 1) {
                        mp++;
                    }
                    if (ayuda + i != sopas.length - 1) {
                        ayuda++;
                    }
                } else {
                    contador = 0;
                    ayuda = 0;
                    mp = 0;
                    k++;
                }

                if (contador == palabraNormal(palabra).length) {
                    ayuda = 0;
                    respuesta[con] = i;
                    respuesta[con + 1] = k;

                    con = con + 2;
                    contador = 0;
                }

            }
            k = 0;

        }

        return respuesta;
    }

    public int contadorVertical(char vector[]) {

        int contador = 0;
        int hecho = 0;
        int k = 0;
        int ayuda = 0;
        int con = 0;

        int mp = 0;
        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {
                if (vector[mp] == sopas[i + ayuda][k]) {
                    contador++;

                    if (mp != vector.length - 1) {
                        mp++;
                    }
                    if (ayuda + i != sopas.length - 1) {
                        ayuda++;
                    }
                } else {
                    contador = 0;
                    ayuda = 0;
                    mp = 0;
                    k++;
                }

                if (contador == vector.length) {
                    hecho++;
                    ayuda = 0;
                }

            }
            k = 0;

        }

        return hecho;
    }

    public int[] diagonalPrincipal(String palabra) {
        int respuesta[] = new int[contadorDiagonalPrincipal(palabraNormal(palabra)) * 2];
        int contador = 0;
        int diagonal = 0;
        int k = 0;
        int mp = 0;
        int con = 0;

        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {
                if (palabraNormal(palabra)[mp] == sopas[i + diagonal][k + diagonal]) {
                    contador++;

                    if (mp != palabraNormal(palabra).length - 1) {
                        mp++;
                    }
                    if (i + diagonal != sopas.length - 1 && k + diagonal != sopas[i].length - 1) {
                        diagonal++;
                    }
                } else {
                    contador = 0;
                    diagonal = 0;
                    mp = 0;
                    k++;
                }

                if (contador == palabraNormal(palabra).length) {
                    contador = 0;
                    respuesta[con] = i;
                    respuesta[con + 1] = k;
                    diagonal = 0;

                    con = con + 2;

                }

            }
            k = 0;
        }

        return respuesta;
    }

    public int[] diagonalPrincipalRevez(String palabra) {

        int respuesta[] = new int[contadorDiagonalPrincipal(palabraAlrevez(palabra)) * 2];
        int contador = 0;
        int diagonal = 0;
        int k = 0;
        int mp = 0;
        int con = 0;

        for (int i = 0; i < sopas.length; i++) {

            while (k < sopas[i].length) {
                if (palabraAlrevez(palabra)[mp] == sopas[i + diagonal][k + diagonal]) {
                    contador++;

                    if (mp != palabraAlrevez(palabra).length - 1) {
                        mp++;
                    }
                    if (i + diagonal != sopas.length - 1 && k + diagonal != sopas[i].length - 1) {
                        diagonal++;
                    }
                } else {
                    contador = 0;
                    diagonal = 0;
                    mp = 0;
                    k++;
                }

                if (contador == palabraAlrevez(palabra).length) {
                    contador = 0;
                    respuesta[con] = i;
                    respuesta[con + 1] = k;
                    diagonal = 0;

                    con = con + 2;

                }

            }
            k = 0;
        }

        return respuesta;
    }

    private int contadorDiagonalPrincipal(char vector[]) {

        int contador = 0;
        int tamanio = 0;
        int k = 0;

        int diagonal = 0;

        for (int i = 0; i < sopas.length; i++) {
            int mp = 0;

            while (k < sopas[i].length) {
                if (vector[mp] == sopas[i + diagonal][k + diagonal]) {
                    contador++;

                    if (mp != vector.length - 1) {
                        mp++;
                    }
                    if (i + diagonal != sopas.length - 1 && k + diagonal != sopas[i].length - 1) {
                        diagonal++;
                    }

                } else {
                    contador = 0;
                    diagonal = 0;
                    mp = 0;
                    k++;
                }

                if (contador == vector.length) {
                    tamanio++;
                    contador = 0;
                    diagonal = 0;
                }

            }
            k = 0;
        }

        return tamanio;
    }

    public char[] palabraAlrevez(String palabra) {
        
        char revez[];
        int j = 0;
        revez = new char[palabra.length()];
        for (int i = palabra.length() - 1; i >= 0; i--) {
            revez[j] = palabra.charAt(i);
            j++;
        }

        return revez;
    }

    public char[] palabraNormal(String palabra) {

        char vector[];
        vector = new char[palabra.length()];
        for (int j = 0; j < palabra.length(); j++) {
            vector[j] = palabra.charAt(j);
        }

        return vector;
    }

    //Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie sopas
     */
    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
